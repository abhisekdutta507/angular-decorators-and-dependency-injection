import { Component } from '@angular/core';
import { StoreService, Topic } from './services/store.service';

/**
 * @description Decorator is prefixed with @ symbol. The parameter passed in decorator is called meta-data.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-decorator';

  topics: Topic[];

  /**
   * @param store injecting dependency in the component.
   */
  constructor(private store: StoreService) {
    /**
     * @description not creating any instance of the StoreService class.
     */
    // const storeInstance = new StoreService();

    this.topics = store.getData();
  }

  // 1
  ngOnInit() {}

  // 2
  ngAfterContentInit() {}

  // 3
  ngAfterViewInit() {}
}
