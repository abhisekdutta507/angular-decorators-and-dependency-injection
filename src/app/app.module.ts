import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BasicDecoratorsComponent } from './components/basic-decorators/basic-decorators.component';
import { SpecialDecoratorsOnFunctionComponent } from './components/special-decorators-on-function/special-decorators-on-function.component';

@NgModule({
  declarations: [
    AppComponent,
    BasicDecoratorsComponent,
    SpecialDecoratorsOnFunctionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
