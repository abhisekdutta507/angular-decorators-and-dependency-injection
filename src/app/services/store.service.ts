import { Injectable } from '@angular/core';

export interface Topic {
  label: string
}

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor() { }

  getData = (): Topic[] => ([
    {
      label: 'Decorators'
    },
    {
      label: 'Dependency Injection (DI)'
    }
  ]) 
}
