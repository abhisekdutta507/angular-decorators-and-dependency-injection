import { Component, OnInit } from '@angular/core';

function readonly(
  target: Object,
  key?: string,
  descriptor?: PropertyDescriptor
) {
  /**
   * @description default writable = true
   */
  descriptor.writable = false;
  return descriptor;
}

function enumerable(value: boolean) {
  return function(
    target: Object,
    key?: string,
    descriptor?: PropertyDescriptor
  ) {
    /**
     * @description default enumerable = false
     */
    descriptor.enumerable = value;
    return descriptor;
  }
}

class Car {

  @readonly
  @enumerable(true)
  wheels() { return 4; }

  tyres() { return 'Apollo'; }

  owner() { return 'Abhisek Dutta'; }

  @enumerable(true)
  brand() { return 'BMW'; }

}

@Component({
  selector: 'special-decorators-on-function',
  templateUrl: './special-decorators-on-function.component.html',
  styleUrls: ['./special-decorators-on-function.component.scss']
})
export class SpecialDecoratorsOnFunctionComponent implements OnInit {

  private car = new Car();

  constructor() {
    try {
      /**
     * @description this is incorrect. Applied read only mata data to it.
     */
      this.car.wheels = () => 3;
    } catch(e) {
      console.error(`ERROR         - ${e.message}`);
    }

    /**
     * @description this is correct.
     */
    this.car.tyres = () => { return 'Michelin'; }
  }

  ngOnInit(): void {
    /**
     * @param wheels is enumerable
     * @param tyres is enumerable
     * @param owner is not enumerable
     * @param brand is enumerable
     */
    const { wheels, tyres, owner, brand } = this.car;

    console.log(`No. of wheels  - ${wheels()}`);
    console.log(`Tyre brand     - ${tyres()}`);
    /**
     * @description the functions with enumerable false value will not be logged.
     */
    for(const key in this.car) {
      console.log(`Car features   - `, key);
    }

  }

}
