import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialDecoratorsOnFunctionComponent } from './special-decorators-on-function.component';

describe('SpecialDecoratorsOnFunctionComponent', () => {
  let component: SpecialDecoratorsOnFunctionComponent;
  let fixture: ComponentFixture<SpecialDecoratorsOnFunctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialDecoratorsOnFunctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialDecoratorsOnFunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
