import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicDecoratorsComponent } from './basic-decorators.component';

describe('BasicDecoratorsComponent', () => {
  let component: BasicDecoratorsComponent;
  let fixture: ComponentFixture<BasicDecoratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicDecoratorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicDecoratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
