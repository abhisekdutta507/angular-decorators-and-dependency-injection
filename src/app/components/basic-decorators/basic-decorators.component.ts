import { Component, OnInit } from '@angular/core';

function logMethod(
  target: Object,
  key?: string,
  descriptor?: PropertyDescriptor
) {
  // storing the actual function in original
  const original = descriptor.value;

  /**
   * @param ...args here Rest operator is used.
   */
  descriptor.value = function (...args: any[]): string {
    // getting the response of the original function
    const result = original.apply(this, args);
    return result;
  }

  return descriptor;
}

export class Planet {
  constructor(public name: string) {}

  /**
   * @description custom decorator applied to a function
   * @param greeting 
   * @param isLoud 
   */
  @logMethod
  greet(greeting: string, isLoud: boolean = false): string {
    const phrase = `${greeting} ${this.name}`;
    return isLoud ? phrase.toUpperCase() : phrase;
  }
}

function logClass(
  target?: any
) {
  Object.defineProperty(target.prototype, 'price', { value: () => '10,000,00 INR' });
}

/**
 * @description custom decorator applied to a class
 */
@logClass
export class Car {
  constructor(public brand?: string) {}

  price(): string {
    return '';
  }
}

@Component({
  selector: 'basic-decorators',
  templateUrl: './basic-decorators.component.html',
  styleUrls: ['./basic-decorators.component.scss']
})
export class BasicDecoratorsComponent implements OnInit {

  greetings: string;

  carPrice: string;

  constructor() { }

  ngOnInit(): void {
    const mars = new Planet('Mars');
    // calling the overridden function
    this.greetings = mars.greet('Welcome to', false);


    const car = new Car();
    // calling the overridden class function
    this.carPrice = car.price();
  }

}
