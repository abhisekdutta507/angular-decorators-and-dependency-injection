This project is created with [Angular CLI](https://angular.io/cli).
We have typescript support in this project using [typescript](https://code.visualstudio.com/docs/typescript/typescript-compiling) with [webpack-cli](https://webpack.js.org/guides/installation/).

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. Then, visit http://localhost:4200 through a browser window to run the application.

## Learn More

You can learn more in the [Angular @Component({...})](https://angular.io/api/core/Component).

### Simple Angular Directives Setup In An Angular Project

**Initialize the project with `NPM`.** *Make sure you have a stable version of* [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/) & [Angular CLI](https://angular.io/cli) *is installed in your system globally.*

```bash
ng new angular-decorator
```

Answer some questions asked by the `Angular CLI` when initiating the new project.

**Check** the `app.component.html` file to understand the directives. Visit [app.component.html](https://bitbucket.org/abhisekdutta507/angular-decorators-and-dependency-injection/src/master/src/app/app.component.ts).

Please visit [@Component](https://angular.io/api/core/Component) for more info.

**Description** - A **component** encapsulates the data, the HTML markup and the logic for a view. That contains the area of a screen the users see. Every Angular app contains at least 1 component that is named AppComponent by default.

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // logical section...
}
```

Please visit [@NgModule](https://angular.io/api/core/NgModule#ngmodule) for more info.

**Description** - A **module** is a container for a group of related components. Every Angular app has 1 module that we call AppModule. We can break an Angular app in smaller modules.

```typescript
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [...],
  imports: [...],
  providers: [...],
  bootstrap: [...]
})
export class AppModule {
    ...
}
```

Please visit [@Injectable](https://angular.io/guide/singleton-services#singleton-services) & [DI](https://angular.io/guide/dependency-injection) for more info.

**Description** - **DI** is useful when we want to share a piece of code throughout our Angular application. We can create a single instance of the service class in the memory. We also call this approach as a Singleton pattern.

Before `Angular 6.0` we had to pass in the service class as a parameter in the `providers` array of its parent `@NgModule`. Now we can pass the `providedIn` attribute in the `@Injectable` decorator.

```typescript
import { Injectable } from '@angular/core';

export interface Topic {
  label: string
}

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor() { }

  getData = (): Topic[] => ([
    {
      label: 'Decorators'
    },
    {
      label: 'Dependency Injection (DI)'
    }
  ]) 
}

```

Checkout the `app.component.ts` too.

```typescript
import { Component } from '@angular/core';
import { StoreService, Topic } from './services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  topics: Topic[];

  /**
   * @param store injecting dependency in the component.
   */
  constructor(private store: StoreService) {
    /**
     * @description not creating any instance of the StoreService class.
     */
    // const storeInstance = new StoreService();

    this.topics = store.getData();
  }
}
```

Please watch [Custom Decorator](https://www.youtube.com/watch?v=Guvd5BYocYg) for more info.

**Description** - TypeScript `Decorator`  is a way to **observe**, **modify**, and **replace** class declarations and members. We can apply decaroators in TypeScript to `classes`, `properties`, `methods`, `accessors (get, set)` and `parameters`.

Learn more [basics](https://bitbucket.org/abhisekdutta507/angular-decorators-and-dependency-injection/src/master/src/app/components/basic-decorators/basic-decorators.component.ts) and with [meta data](https://bitbucket.org/abhisekdutta507/angular-decorators-and-dependency-injection/src/master/src/app/components/special-decorators-on-function/special-decorators-on-function.component.ts).

```typescript
function readonly(
  target: Object,
  key?: string,
  descriptor?: PropertyDescriptor
) {
  /**
   * @description default writable = true
   */
  descriptor.writable = false;
  return descriptor;
}

class Car {
  @readonly
  wheels() { return 4; }

  tyres() { return 'Apollo'; }
}

...

private car = new Car();

try {
  /**
 * @description this is incorrect. Applied read only mata data to it.
 */
  this.car.wheels = () => 3;
} catch(e) {
  console.error(`ERROR         - ${e.message}`);
}

/**
 * @description this is correct.
 */
this.car.tyres = () => { return 'Michelin'; }
```

Hurray!! we have learned the basic concepts of Angular decorators & dependency injection.